import pyspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
import time

SPARK_MODULES = [
    "org.apache.hadoop:hadoop-aws:3.2.0",
    "com.amazonaws:aws-java-sdk-bundle:1.11.375"
]

def spark_config():
    # Can also specify executor/driver memory, number of executors, etc.
    return {
        ("spark.jars.packages", ",".join(SPARK_MODULES)),
        ("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem"),
        ("spark.hadoop.fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.AnonymousAWSCredentialsProvider"),
    }   

def create_spark_session():
    spark = SparkSession.builder.appName("spark-on-k8s") \
        .config(conf=pyspark.SparkConf().setAll(spark_config())) \
        .getOrCreate()
    return spark

def read_sample_data(spark):
    """Reads a csv from s3"""
    dataset = "s3a://datasets-documentation/nyc-taxi/*.gz"
    df = spark.read.csv(dataset, header=True, inferSchema=True, sep="\t")
    df = df.repartition(50, "trip_id")
    df.collect()
    return df

def process_data(df):
    """Processes the data"""
    return df.select("pickup_date", "total_amount") \
        .orderBy(F.col("total_amount").desc()) \
        .take(1)

def main():
    spark = create_spark_session()
    df = read_sample_data(spark)
    df.show()
    result = process_data(df)
    print(result)
    #time.sleep(180)
    spark.stop()
    
if __name__ == "__main__":
    main()
